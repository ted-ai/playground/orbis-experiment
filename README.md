# Knowledge Graph for Public Procurement Fraud Detection

## 1 Introduction
Embarking on a challenge to detect and prevent fraud in public procurement, this project introduces the creation of a sophisticated knowledge graph designed to enhance fraud detection. By leveraging the detailed `OP-Orbis_data_product` database, our aim is to identify irregularities and patterns indicative of fraud and decipher complex data patterns in company and organizational transactions within public contracts. 

This project harnesses advanced analytics and knowledge graph technology to bolster transparency and combat fraud in public procurement, contributing to the development of improved practices in the sector.

## 2 Methodology

The methodology of this project revolves around the meticulous analysis and utilization of the `OP-Orbis_data_product` database. This comprehensive database serves as the backbone of our knowledge graph, providing a wealth of firmographic and financial information critical for detecting fraudulent activities in public procurement processes.

### 2.1 Database Description and Structure

The `OP-Orbis_data_product` database is segmented into several key components, each offering different lenses through which a company's data can be scrutinized:

- **Firmographics**: This section contains detailed company records, including identifiers, address information, contact details, industry classifications, legal info, and overviews.
<div align="center">
  <img src="images/databaseUML/firmographics.png" width="50%" height="50%">
</div>

- **Key Financials**: Financial data is a crucial element of our analysis, encompassing high-level financial details such as profit and loss accounts, cash flow, balance sheets, and standard financial ratios like ROE and ROCE. This dataset is divided into three segments, providing financial information in original currency, US dollars, and euros, allowing for a standardized comparison across different monetary units.
<div align="center">
  <img src="images/databaseUML/key_financials.png" width="50%" height="50%">
</div>

- **Key Ownership**: Ownership information illuminates the shareholding patterns and control structures within companies. It includes data on basic shareholders, all current shareholders at the first level, and details of controlling stakeholders, offering insights into the corporate governance that may affect the integrity of public procurement processes.
<div align="center">
  <img src="images/databaseUML/key_ownership.png" width="50%" height="50%">
</div>

- **People**: This component captures connections to individuals, including current and previous directors, auditors, bankers, and other advisors. It provides a human element to the data, revealing potential conflicts of interest or networks that could be leveraged for fraudulent purposes.
<div align="center">
  <img src="images/databaseUML/people.png" width="50%" height="50%">
</div>

Each of these components is interconnected through a relational schema that allows for complex queries and analysis. Primary keys (PK) and foreign keys (FK) are used to ensure data integrity and relational mapping across different tables. The database's structure is designed to support advanced data mining techniques and sophisticated algorithmic interrogation, enabling the detection of subtle and complex fraud indicators that might otherwise go unnoticed.

The structure and depth of the `OP-Orbis_data_product` database make it a powerful tool in the fight against fraud in public procurement. By analyzing patterns across firmographics, financials, ownership, and personal associations, we can construct a knowledge graph that is not only comprehensive but also capable of revealing the multi-dimensional nature of procurement fraud.

### 2.2 Tools Used

- [Morph-KGC](https://github.com/morph-kgc/morph-kgc): This tool is pivotal for transforming non-RDF data into Knowledge Graphs. Morph-KGC interprets mappings from source data to RDF (Resource Description Framework) using the YARRRML mapping language. This allows for the creation of semantic data models which are essential for integrating, querying, and managing the data within the knowledge graph.

- [YARRRML](https://rml.io/yarrrml/): A human-friendly language for specifying mappings from raw data to RDF. It's used to define how each piece of information within the database corresponds to unique elements within the RDF, which is the foundation of our knowledge graph. YARRRML's intuitive syntax and powerful directives facilitate complex mappings that are both maintainable and scalable.

- [RDF](https://www.w3.org/RDF/): The RDF is a standard model for data interchange on the web. It provides a flexible way to represent information about resources in a graph form. In our project, RDF is used to encode, exchange, and reuse structured data, and serves as a cornerstone for querying with SPARQL, enabling sophisticated analytics on the interlinked data.

- [Virtuoso](https://vos.openlinksw.com/owiki/wiki/VOS/VOSRDF): An advanced multi-model data server used for storing, managing, and querying RDF data, Virtuoso has been instrumental for our testing phases. It allows us to visualize and interact with the knowledge graphs, facilitating complex queries and analyses.

Each of these tools plays a crucial role in the construction and operation of the knowledge graph, enabling the seamless conversion of data into actionable insights for fraud detection in public procurement.

### 2.3 Process of generating the Knowledge Graph

The construction of the Knowledge Graph was a multi-stage process that involved the integration of various data sources, transformation of data into a semantic format, and the enrichment of the graph to facilitate advanced queries and analytics.

1. **Data Integration**: Initially, data from the `OP-Orbis_data_product` database was consolidated, ensuring that all relevant firmographic, financial, ownership, and personal data were included. This step was crucial for providing a comprehensive base for the knowledge graph.

2. **Data Transformation**: Using Morph-KGC, the raw data was transformed into RDF triples, a process guided by YARRRML mappings. These mappings dictated the conversion rules from the database's schema to RDF, ensuring each data point was accurately represented in the graph.

3. **Semantic Enrichment**: The RDF triples were then semantically enriched with ontologies that provide context and meaning to the data. This enrichment allowed for the data to be interconnected in ways that facilitate complex reasoning and analytics.

4. **Graph Construction**: With the enriched RDF data, the Knowledge Graph was constructed. This graph is a network of interlinked data points, representing not only the data entities but also the relationships between them.

5. **Querying and Analysis**: Using Virtuoso, the graph was queried and analyzed. SPARQL, the RDF query language, was used to explore the data, test hypotheses about potential fraud patterns, and extract insights.

6. **Visualization and Interaction**: Throughout the process, visualization tools were employed to interact with the Knowledge Graph. This allowed for a more intuitive understanding of the data and helped in identifying patterns that signify fraudulent activities.

7. **Iterative Refinement**: The Knowledge Graph was iteratively refined, with new data added and mappings adjusted, to improve the accuracy and relevance of the information it represented.

This process resulted in a dynamic Knowledge Graph that serves as a powerful tool for detecting fraud within public procurement by revealing hidden relationships and patterns in the data.


## 3. Project Development

The project development phase was meticulously organized to establish a robust Knowledge Graph capable of identifying fraudulent activities within public procurement processes. The following outlines the key stages:

1. **Data Comprehension**: Given that the `.parquet` files were already generated, this stage focused on comprehending the data. It entailed visualizing, analyzing, and understanding the content of each `.parquet` file to ensure a thorough grasp of the data's nature, structure, and interrelationships. This deep comprehension was critical for accurate YARRRML rule definition and effective data transformation.

2. **Definition and Implementation of YARRRML Rules**: With a solid understanding of the data, YARRRML rules were crafted to facilitate the transformation of the `.parquet` data into an RDF format. Access to the data was achieved through SQL queries. The rules were divided into separate files for better management: one set for the base `OP-Orbis_data_product` database and another for contract-related data, which was sourced from the `fin_match_output.xlsx` file, to allow for easier updates and modular maintenance.

3. **Generation of Separate Knowledge Graphs**: Adhering to the YARRRML mappings, two discrete Knowledge Graphs were produced. The first graph mapped out the core data, including firmographic and financial information, while the second graph focused on the specifics of procurement contracts.

4. **Combination of Knowledge Graphs**: These separate graphs were then meticulously combined to form a comprehensive Knowledge Graph. This step was executed with precision to ensure the integrity of the data and the relationships within the unified graph.

5. **Specifications about Transformations and Joins Performed**: Detailed specifications were outlined for the transformations and joins, which were crucial for correlating entities and constructing a connected web of data. These specifications were key to enabling the detection of complex fraud indicators within the procurement data.

6. **Iterative Testing and Refinement**: The Knowledge Graph underwent iterative testing and refinement to validate its accuracy and utility. This process involved executing test queries and refining the transformation rules and join logic to enhance the graph's quality.


## 4. Results

### 4.1 Architecture of the Knowledge Graph

The architecture of the Knowledge Graph is engineered to be robust and scalable, with a focus on capturing the multifaceted nature of public procurement data. The structure is derived from two primary YARRRML mapping files which define how data is transformed and interlinked within the graph.

#### 4.1.1 YARRRML Mappings

- **Base Database Mappings**: The first YARRRML file dictates the transformation rules for the base `OP-Orbis_data_product` database. It lays out the RDF triples for firmographics, financial data, and other foundational elements. These mappings ensure that data from the `.parquet` files is accurately represented within the Knowledge Graph, providing a structured and queryable format.

- **Contracts Mappings**: The second YARRRML file pertains to the contract-related data, sourced from the `fin_match_output.xlsx` file. This set of rules is specifically designed to address the nuances of procurement contracts, translating them into a format that the Knowledge Graph can assimilate and analyze in conjunction with the base data.

#### 4.1.2 Graph Structure

The outcomes of the project are visualized through the final structure of the Knowledge Graph, which has been carefully constructed and refined. Below are the key components, each demonstrated with an illustrative image. Notably, the "Identifiers" component was omitted due to the absence of corresponding data in the `OP-Orbis_data_product` database, but the remaining structure operates as intended.

<div align="center">
  <img src="images/solutionUML/ALL_SOLUTION_summary.png" width="100%" height="100%">
</div>

### 4.2 SPARQL Query Examples and Descriptive Analysis

To demonstrate the effectiveness and capabilities of the Knowledge Graph, several query examples were executed, each designed to reveal specific types of relationships or anomalies that could indicate potential fraud or irregularities within public procurement processes.

#### 4.2.1 Only OP-Orbis_data_product Knowledge Graph

##### 4.2.1.1. Companies related due to common owners

A query to uncover companies linked through shared ownership, a situation that may raise concerns about conflicts of interest or collusion.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-1-1.png" width="50%" height="50%">
</div>

There are 1,379 unique companies that have shared ownership connections, this group constitutes about 14% of the total number of companies, highlighting a web of potential interconnectedness through shared owners.

##### 4.2.1.2. Companies that depend on others and their principal owners

This query identified subsidiary companies and their principal owners, providing insights into the control dynamics within corporate groups.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-1-2.png" width="50%" height="50%">
</div>

Of the 9,814 companies represented in the graph, 1,239 are subsidiaries, accounting for roughly 12.6% of the total. These subsidiary entities are under the control of 829 parent companies, which themselves amount to approximately 8.4% of all companies in the graph. This subset of companies forms a network of control dynamics, with 1.648 principal owners, each potentially influencing multiple entities within the corporate groups. 

##### 4.2.1.3. Companies with a high number of subsidiaries with the same name

Such a pattern could indicate complex corporate structures that might be used to obfuscate ownership or financial flows.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-1-3.png" width="50%" height="50%">
</div>

The analysis of the query results concerning companies with a high number of subsidiaries sharing the same name has revealed that within the knowledge graph, 926 unique parent companies have been documented. However, the data does not indicate instances where a single parent company has multiple subsidiaries with the exact same name, as the maximum number of subsidiaries with the same name is noted to be one.

##### 4.2.1.4. Companies with a recent change of ownership

Rapid changes in ownership can be a red flag for fraudulent activities, potentially indicating attempts to hide the true beneficiaries of contracts.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-1-4.png" width="50%" height="50%">
</div>

Based on the finding that 2,836 companies, or a substantial 29% of the total entities, have altered their names during 2023.

##### 4.2.1.5. Companies with Frequent Ownership Changes

Frequent changes could suggest instability or attempts at regulatory evasion.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-1-5.png" width="50%" height="50%">
</div>

Only 0.34% of the companies have changed their name on 6 or more occasions. This is a very small number of companies, but it is still important to investigate these cases further.

##### 4.2.1.6. Companies with a history of delisting and relisting

This pattern may signal financial or legal issues that merit further investigation.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-1-6.png" width="50%" height="50%">
</div>

Out of 9,814 companies, 91 have experienced delisting and relisting events in 2023, representing approximately 0.9% of the total, indicating potential financial or legal issues in this specific year.

##### 4.2.1.7. Companies with conflicting or inconsistent financial information

Inconsistencies in financial reporting can be a significant indicator of fraudulent financial manipulation or reporting errors.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-1-7.png" width="50%" height="50%">
</div>

The identification of companies with conflicting or inconsistent financial information highlights the critical need for thorough financial scrutiny. Although this analysis does not delve into specific interpretations, it emphasizes the importance of detecting and addressing potential financial irregularities.

##### 4.2.1.8. Companies with significant changes in their registered addresses and names

Frequent changes might indicate a company's attempt to avoid scrutiny or conceal past activities.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-1-8.png" width="50%" height="50%">
</div>

The analysis of companies with significant changes in their registered addresses and names reveals noteworthy patterns. A few companies, although constituting a small portion of the overall dataset, exhibit a high frequency of address changes, with one company changing addresses up to 51 times. This pattern, along with multiple name changes, could potentially signal efforts to elude regulatory oversight or conceal previous activities, necessitating closer examination to understand the underlying reasons for these frequent modifications.

##### 4.2.1.9. Companies with a high number of directors who are also shareholders

This situation could point to a lack of independent oversight, which is often critical for good corporate governance.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-1-9.png" width="50%" height="50%">
</div>

The dual role of directors as shareholders, seen in a small percentage of the dataset (0.22%), raises potential concerns about the independence of the board and the effective separation of oversight and financial interests.

##### 4.2.1.10. Companies with frequent changes in their primary business lines or core activities 

Such changes might suggest a company is trying to diversify rapidly, potentially spreading risk due to fraudulent activities.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-1-10.png" width="50%" height="50%">
</div>

Out of 9,814 companies, only a few met the query's criteria of changing their primary business lines or core activities at least 15 times, indicating a rare but notable pattern. These frequent changes, though affecting a small fraction of the dataset (0.1%), might suggest strategic adaptations or efforts to obscure business practices, warranting further investigation in the context of fraud detection.

#### 4.2.2 Adding Contracts Knowledge Graph

##### 4.2.2.1. From those companies that got a contract, check those that had changes in their owners

This query targets companies that have won contracts and subsequently experienced ownership changes, which could indicate a transfer of control following contract awards, possibly to obscure the beneficiaries.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-2-1.png" width="50%" height="50%">
</div>

It's found that out of 1,049 total contracts, 765 unique contracts involve companies that underwent ownership changes, accounting for approximately 72.9% of all contracts. This significant proportion suggests potential transfer of control post-contract award.

##### 4.2.2.2. Get the companies that obtained more contracts.

Identifying companies that have secured a large number of contracts may point to monopolistic practices or preferential treatment within procurement processes.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-2-2.png" width="50%" height="50%">
</div>

For the query targeting companies that obtained a notable number of contracts, results indicate a concentration of contracts among a few companies. A single company secured as many as 42 contracts (4%), while others secured between 30 to 40 contracts (2.8%-3.8%). This pattern, evident in the data, raises concerns about potential monopolistic practices or preferential treatment within procurement processes, especially considering that the majority of companies secured fewer than 5 contracts out of a total of 1,049 contracts.

##### 4.2.2.3. Companies winning contracts in various geographic locations

This query explores the geographic spread of contract-winning companies, potentially uncovering strategies for market domination or detecting entities that operate in jurisdictions with less stringent regulations.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-2-3.png" width="50%" height="50%">
</div>

Although no company has won contracts in more than one country, the number of contracts secured reveals significant concentration among certain companies. For instance, one company stands out with 327 contracts, markedly more than others on the list. This may indicate monopolistic practices or preferential treatment in procurement processes, thereby limiting market competition. The observation that companies are not operating across multiple geographies might reflect country-specific market strategies or localized award preferences.

##### 4.2.2.4. Contract-winning companies with significant changes in executive management

Significant executive turnover in companies that frequently win contracts could be symptomatic of internal instability or attempts to renew corrupt practices by changing leadership.

<div align="center">
  <img src="images/screenshoots_anonymous_query_results/a-2-4.png" width="50%" height="50%">
</div>

The results indicate a significant intersection between companies frequently winning contracts and those experiencing notable executive changes. A particular company, with 327 contracts, also had a substantial 226 executive changes, amounting to approximately 69% of its contracts associated with management shifts. This pattern is observed in other companies as well, where high contract counts align with numerous executive changes. Such a trend might reflect internal organizational instability or deliberate restructuring strategies, potentially as a means to refresh management in response to new contract opportunities or to obscure previous management's involvement in activities requiring scrutiny.

## 5. Conclusions
The Knowledge Graph for Public Procurement Fraud Detection has established a solid foundation for advanced data analysis, demonstrating substantial potential in identifying and preventing fraudulent activities. With its comprehensive construction based on the `OP-Orbis_data_product` database, the project has showcased the power of integrating diverse datasets to reveal intricate patterns and relationships.

Looking ahead, the Knowledge Graph is set for deployment on Amazon Neptune, which will likely enhance its efficacy. Amazon Neptune's attributes of being fast, reliable, and scalable are poised to elevate the graph's performance, particularly in processing complex queries and integrating with a broad array of AWS services. This advancement is expected to amplify the Knowledge Graph's capabilities, particularly in real-time data analysis, which is crucial for prompt fraud detection and action in the ever-evolving domain of public procurement.

## Appendix

- **Project Source Code**: All the source code and scripts for this project are available at our GitHub repository: [AI Companies Knowledge Graph Project](https://github.com/Jaime-gc-00/ppds-ai-companies-kg.git).

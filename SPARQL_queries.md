# 1 Query examples

## 1.1. Companies related due to common owners

```
PREFIX ex: <http://example.com/>

SELECT ?CompanyName1 ?CompanyName2 ?ShareholderID
WHERE {
    ?company1 a ex:Company ;
              ex:NAME_INTERNATIONAL ?CompanyName1 ;
              ex:has_controlling_shareholders ?shareholders1 .
    ?shareholders1 ex:CSH_BVD_ID_NUMBER ?ShareholderID .

    ?company2 a ex:Company ;
              ex:NAME_INTERNATIONAL ?CompanyName2 ;
              ex:has_controlling_shareholders ?shareholders2 .
    ?shareholders2 ex:CSH_BVD_ID_NUMBER ?ShareholderID .

    FILTER (?company1 != ?company2)
}
```

## 1.2. Companies that depend on others and their principal owners

```
PREFIX ex: <http://example.com/>

SELECT ?ParentCompanyName ?SubsidiaryCompanyName ?PrincipalOwnerBVDID
WHERE {
    # Find parent companies and their subsidiary BVD ID numbers
    ?parentCompany a ex:Company ;
                   ex:NAME_INTERNATIONAL ?ParentCompanyName ;
                   ex:has_all_subsidiaries_first_level ?subsidiaryLink .
    ?subsidiaryLink ex:SUB_BVD_ID_NUMBER ?SubsidiaryBVDID .

    # Get the international names of the subsidiaries using their BVD ID numbers
    ?subsidiary a ex:Company ;
                ex:BVD_ID_NUMBER ?SubsidiaryBVDID ;
                ex:NAME_INTERNATIONAL ?SubsidiaryCompanyName .

    # Find BVD ID numbers of the principal owners of the subsidiary companies
    ?subsidiary ex:has_controlling_shareholders ?shareholders .
    ?shareholders ex:CSH_BVD_ID_NUMBER ?PrincipalOwnerBVDID .
}
```

## 1.3. Companies with a high number of subsidiaries with the same name

```
PREFIX ex: <http://example.com/>

SELECT ?ParentCompanyName ?SubsidiaryName (COUNT(?SubsidiaryName) AS ?NumberOfSubsidiaries)
WHERE {
    # Find parent companies and their subsidiaries
    ?parentCompany a ex:Company ;
                   ex:NAME_INTERNATIONAL ?ParentCompanyName ;
                   ex:has_all_subsidiaries_first_level ?subsidiaryLink .
    ?subsidiaryLink ex:SUB_BVD_ID_NUMBER ?SubsidiaryBVDID .

    # Get the names of the subsidiaries using their BVD ID numbers
    ?subsidiary a ex:Company ;
                ex:BVD_ID_NUMBER ?SubsidiaryBVDID ;
                ex:NAME_INTERNATIONAL ?SubsidiaryName .
}

GROUP BY ?ParentCompanyName ?SubsidiaryName
HAVING (COUNT(?SubsidiaryName) > 0) # MODIFY THRESHOLD AS NEEDED
ORDER BY DESC(?NumberOfSubsidiaries)
```

## 1.4. Companies with a recent change of ownership

```
PREFIX ex: <http://example.com/>

SELECT ?CompanyName ?LastChangeDate
WHERE {
    # Find companies and the date of the last ownership change
    ?company a ex:Company ;
             ex:NAME_INTERNATIONAL ?CompanyName ;
             ex:has_controlling_shareholders ?shareholders .
    ?shareholders ex:CSH_INFORMATION_DATE ?LastChangeDate .

    # Filter for changes since some date
    FILTER (STR(?LastChangeDate) >= "2023/01")
}
ORDER BY DESC(?LastChangeDate)
```

## 1.5. Companies with Frequent Ownership Changes

```
PREFIX ex: <http://example.com/>
SELECT ?CompanyName (COUNT(?ChangeDate) AS ?NumberOfChanges)
WHERE {
    ?company a ex:Company ;
             ex:NAME_INTERNATIONAL ?CompanyName ;
             ex:has_controlling_shareholders ?shareholders .
    ?shareholders ex:CSH_INFORMATION_DATE ?ChangeDate .
}
GROUP BY ?CompanyName
HAVING (COUNT(?ChangeDate) > 5) # MODIFY THRESHOLD AS NEEDED
ORDER BY DESC(?NumberOfChanges)
```

## 1.6. Companies with a history of delisting and relisting

```
PREFIX ex: <http://example.com/>
SELECT ?CompanyName ?DelistedDate ?RelistedDate
WHERE {
    ?company a ex:Company ;
             ex:NAME_INTERNATIONAL ?CompanyName ;
             ex:has_legal_info ?legalInfo .
    ?legalInfo ex:SD_DELISTED_DATE ?DelistedDate ;
               ex:IPO_DATE ?RelistedDate .

    FILTER (STR(?DelistedDate) != "NaT" && STR(?RelistedDate) != "NaT")
}
```

## 1.7. Companies with conflicting or inconsistent financial information

```
PREFIX ex: <http://example.com/>
SELECT ?CompanyName ?FinancialYear ?Revenue ?Profit
WHERE {
    ?financials a ex:key_financials ;
                ex:CLOSING_DATE ?FinancialYear ;
                ex:OPRE ?Revenue ;
                ex:PL ?Profit .
    ?company ex:has_key_financials ?financials ;
             ex:NAME_INTERNATIONAL ?CompanyName .
    # Add filters to identify significant variations
}
```

## 1.8. Companies with significant changes in their registered addresses and names

```
PREFIX ex: <http://example.com/>
SELECT ?CompanyName (COUNT(DISTINCT ?PreviousName) AS ?NumberOfNameChanges) (COUNT(DISTINCT ?PreviousAddress) AS ?NumberOfAddressChanges)
WHERE {
    ?company a ex:Company ;
             ex:NAME_INTERNATIONAL ?CompanyName ;
             ex:has_additional_company_info ?additionalInfo ;
             ex:has_all_addresses ?addressInfo .

    # Counting name changes
    OPTIONAL {
        ?additionalInfo ex:PREVIOUS_NAME ?PreviousName .
    }

    # Counting changes of address
    OPTIONAL {
        ?addressInfo ex:ADDRESS_LINE1_ADDITIONAL_INTERNATIONAL ?PreviousAddress .
    }
}
GROUP BY ?CompanyName
HAVING (COUNT(DISTINCT ?PreviousName) > 1 && COUNT(DISTINCT ?PreviousAddress) > 4) # MODIFY THRESHOLDS AS NEEDED
ORDER BY DESC(?NumberOfAddressChanges)
```

## 1.9. Companies with a high number of directors who are also shareholders

```
PREFIX ex: <http://example.com/>

SELECT ?CompanyName (COUNT(?DirectorShareholder) AS ?NumberOfDirectorShareholders)
WHERE {
    # Identify companies
    ?company a ex:Company ;
             ex:NAME_INTERNATIONAL ?CompanyName ;
             ex:has_dmc_current_only ?management .
    
    # Identify directors who are also shareholders
    ?management ex:CPYCONTACTS_MEMBERSHIP_IsAShareholderFormatted ?DirectorShareholder .
    
    FILTER (?DirectorShareholder = "Yes") 
}
GROUP BY ?CompanyName
HAVING (COUNT(?DirectorShareholder) > 1) # MODIFY THRESHOLD AS NEEDED
ORDER BY DESC(?NumberOfDirectorShareholders)
```

## 1.10. Companies with frequent changes in their primary business lines or core activities 

```
PREFIX ex: <http://example.com/>

SELECT ?CompanyName (COUNT(?BusinessLineChange) AS ?NumberOfChanges)
WHERE {
    # Find companies and their primary business line or core activity
    ?company a ex:Company ;
             ex:NAME_INTERNATIONAL ?CompanyName ;
             ex:has_industry_classifications ?industryClassifications .
    
    OPTIONAL {
        ?industryClassifications ex:INDUSTRY_PRIMARY_CODE ?BusinessLineChange .
    }
}
GROUP BY ?CompanyName
HAVING (COUNT(?BusinessLineChange) > 14) # MODIFY THRESHOLD AS NEEDED
ORDER BY DESC(?NumberOfChanges)
```


# 2 Contract Example Queries

## 2.1. From those companies that got a contract, check those that had changes in their owners

```
PREFIX ex: <http://example.com/>

SELECT ?contract ?CompanyName ?LastChangeDate
WHERE {

    ?contract a ex:Contract ;
              ex:hasWinner ?company .
    ?company ex:NAME_INTERNATIONAL ?CompanyName .

    ?company ex:has_controlling_shareholders ?shareholders .
    ?shareholders ex:CSH_INFORMATION_DATE ?LastChangeDate .
}
ORDER BY ?contract
```

## 2.2. Get the companies that obtained more contracts.

```
PREFIX ex: <http://example.com/>

SELECT ?CompanyID ?CompanyName (COUNT(?contract) AS ?NumberOfContracts)
WHERE {

    ?contract a ex:Contract ;
              ex:hasWinner ?company .
    ?company ex:BVD_ID_NUMBER ?CompanyID ;
             ex:NAME_INTERNATIONAL ?CompanyName .
}
GROUP BY ?CompanyID ?CompanyName
ORDER BY DESC(?NumberOfContracts)

```

## 2.3. Companies winning contracts in various geographic locations

```
PREFIX ex: <http://example.com/>

SELECT ?CompanyName (COUNT(DISTINCT ?ContractCountry) AS ?NumberOfDifferentCountries) (COUNT(?contract) AS ?TotalContracts)
WHERE {
    ?contract a ex:Contract ;
              ex:hasWinner ?company ;
              ex:contract_country ?ContractCountry .
    ?company ex:NAME_INTERNATIONAL ?CompanyName .
}
GROUP BY ?CompanyName
HAVING (COUNT(DISTINCT ?ContractCountry) > 1) # Companies must have won contracts in more than one country.
ORDER BY DESC(?NumberOfDifferentCountries)
```

## 2.4. Contract-winning companies with significant changes in executive management

```
PREFIX ex: <http://example.com/>

SELECT ?CompanyName (COUNT(?contract) AS ?NumberOfContracts) (COUNT(?executiveChange) AS ?NumberOfExecutiveChanges)
WHERE {

    ?contract a ex:Contract ;
              ex:hasWinner ?company .
    ?company ex:NAME_INTERNATIONAL ?CompanyName .

    OPTIONAL {
        ?company ex:has_dmc_current_only ?executiveChange .
        ?executiveChange ex:CPYCONTACTS_MEMBERSHIP_BeginningNominationDate ?ChangeDate .
    }

}
GROUP BY ?CompanyName
HAVING (COUNT(?contract) > 10 AND COUNT(?executiveChange) > 1)
ORDER BY DESC(?NumberOfContracts), DESC(?NumberOfExecutiveChanges)
```
